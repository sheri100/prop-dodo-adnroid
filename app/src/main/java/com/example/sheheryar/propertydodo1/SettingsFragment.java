package com.example.sheheryar.propertydodo1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.TextView;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

public class SettingsFragment extends Fragment {

    public SettingsFragment() {
    }

    public static SettingsFragment newInstance() {
        SettingsFragment fragment = new SettingsFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        final android.app.AlertDialog.Builder popDialog = new android.app.AlertDialog.Builder(getActivity());
        final LayoutInflater inflaters = (LayoutInflater) getActivity().getSystemService(LAYOUT_INFLATER_SERVICE);
        final android.app.AlertDialog ad = popDialog.create();


        TextView t1 = (TextView) rootView.findViewById(R.id.currency_id);
        t1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buildDialog(R.style.DialogAnimation, "Fade In - Fade Out Animation!");
            }
        });


        return rootView;
    }

    private void buildDialog(int animationSource, String type) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Animation Dialog");
        builder.setMessage(type);
        builder.setNegativeButton("OK", null);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = animationSource;
        dialog.show();
    }
}
